const express = require('express');

const app = express();
const PORT = 3000;
const path = require('path');

//request route on the root URL
app.get('/', (req, res) => {
	res.status(200);
	res.send("Hellow World");
	}
);

//request route on the root/public URL
app.get('/public', function(req, res) {
	res.sendFile(path.join(__dirname + '/index.html'));
	}
);

//request route on the root/about URL
app.get('/about', (req, res) => {
	res.status(200);
	res.send("Это простое приложение Express.js");
	}
);

//processing a missing route
app.get('*', (req, res) => {
	res.status(404);
	res.send("Страница не найдена");
	}
);


app.listen(PORT, (error) => {
	if(!error)
		console.log("Listening port " + PORT);
	else
		console.log("Error ", error);
	}
);

