# FirstTask

## Описание проекта

Данный проект содержит базовое веб-приложение, написанное с использованием фреймворка Express.js для Node.js, которое обслуживает статические файлы и обрабатывает несколько маршрутов.

Приложение написано для решение задания для стажировки.

## Инструкция по запуску

Для запуска откройте командную строку в папке с приложением и выполните одну из следующих команд:

```
node app.js
```
или

```
npm start
```

Если все хорошо, то в консоли появится сообщение:

```
Listening port 3000
```

После этого в своем бразуре вы сможете перейти на следующие странички:

```
http://localhost:3000
http://localhost:3000/public
http://localhost:3000/about
```

## Краткое описание содержимого проекта

В файле app.js хранятся обработчки маршрутов '/', '/public' и '/about', а так же обработчик перехода на отсутствующую страницу.

Файл 'index.html' содержит стиль тсранички '/public'

